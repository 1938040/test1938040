package q4;

import java.util.*;

import q3.Planet;

public class CollectionsMethods {
	public static Collection<Planet> getLargerThan(Collection<Planet> planets,double size){
		List<Planet> returnplanets=new ArrayList<Planet>();
		
		for(Planet p:planets) {
			if(p.getRadius()>=size) {
				returnplanets.add(p);
			}
		}
		return returnplanets;
	}
}
