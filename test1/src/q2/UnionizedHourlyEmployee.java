package q2;

public class UnionizedHourlyEmployee extends HourlyEmployee {
	private int maxHoursPerWeek;
	private double overtimeRate;
	
	public UnionizedHourlyEmployee(int hoursworked, double hourlypay,int maxHoursPerWeek, double overtimeRate) {
		super(hoursworked,hourlypay);
		this.maxHoursPerWeek=maxHoursPerWeek;
		this.overtimeRate=overtimeRate;
	}
	
	public int getmaxHoursPerWeek() {
		return maxHoursPerWeek;
	}
	public double getovertimeRate() {
		return overtimeRate;
	}
	
	@Override
	public double getWeeklyPay() {
		if(this.gethoursworked()<=maxHoursPerWeek) {
			return this.gethourlypay()*this.gethoursworked();
		}
		else {
			int overtime=this.gethoursworked()-maxHoursPerWeek;
			return maxHoursPerWeek*this.gethourlypay()+overtime*overtimeRate;
		}
	}
}
