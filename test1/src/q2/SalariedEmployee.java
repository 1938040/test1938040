package q2;

public class SalariedEmployee implements Employee{
	private double yearly;
	
	public SalariedEmployee(double yearly) {
		this.yearly=yearly;
	}
	
	public double getWeeklyPay() {
		return yearly/52;
	}
	
	public double getYearly() {
		return yearly;
	}
}
