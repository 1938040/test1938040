package q2;

public class HourlyEmployee implements Employee{
	private int hoursworked;
	private double hourlypay;
	
	public double getWeeklyPay() {
		return hoursworked*hourlypay;
	}
	
	public HourlyEmployee(int hoursworked, double hourlypay) {
		this.hoursworked=hoursworked;
		this.hourlypay=hourlypay;
	}
	
	public int gethoursworked() {
		return hoursworked;
	}
	public double gethourlypay() {
		return hourlypay;
	}
	
}
