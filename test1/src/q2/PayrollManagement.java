package q2;

public class PayrollManagement {
	public static void main(String []args) {
		HourlyEmployee a=new HourlyEmployee(30,20.5);
		HourlyEmployee b=new HourlyEmployee(15,20.5);
		UnionizedHourlyEmployee c=new UnionizedHourlyEmployee(30,20.5,40,25);
		SalariedEmployee d=new SalariedEmployee(30020.5);
		SalariedEmployee e=new SalariedEmployee(30060.5);
		
		
		Employee[] employeesarr= {a,b,c,d,e};
		
		System.out.println(getTotalExpenses(employeesarr));
	}
	
	
	public static double getTotalExpenses(Employee[] employeesarr) {
		
		double expenses=0;
		
		for(Employee e:employeesarr) {
			expenses+=e.getWeeklyPay();
		}
		return expenses;
	}
}
